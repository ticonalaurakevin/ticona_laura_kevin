#include "chess.h"
#include "figures.h"

void display(){
	char* ej4 = repeatH(join(reverse(whiteSquare),whiteSquare),4);
	char* piezas1 = join(rook,join(knight,bishop));
	char* piezas0 = join(rook,join(flipV(knight),bishop));
	char* reyes = join(queen,king);
	char* piezas2 = join(piezas1,join(reyes,flipV(piezas0)));
	char* a = superImpose(piezas2,ej4);
 	interpreter(a);
}

