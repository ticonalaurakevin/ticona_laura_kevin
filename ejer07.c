#include "chess.h"
#include "figures.h"

void display(){

	char* negro = reverse(whiteSquare);
	char* neg = repeatH(join(reverse(whiteSquare),whiteSquare),4);

	char* neg2 = join(negro,whiteSquare);
	char* bla2 = reverse(neg2);
	
	char* bla = reverse(neg);

	char* bla6 = repeatH(bla2,3);
	char* neg6 = repeatH(neg2,3);
	char* neg4 = repeatH(neg2,2);

	char* qnegra = superImpose(queen,negro);
	char* qblanca = superImpose(queen,whiteSquare);

	char* sinpiezas = repeatV(up(neg,bla),4);
	char* reyna1 = join(whiteSquare,join(qnegra,bla6));
	char* reyna2 = join(neg6,join(qnegra,whiteSquare));

	char* reyna21 = flipV(reyna1);

	char* reyna3 = join(bla2,join(qblanca,neg4));
	char* reyna31 = join(reyna3,negro);

	char* reyna4 = flipV(reyna31);

	char* reyna5 = join(bla6,join(whiteSquare,qnegra));	
	
	char* reyna6 = join(neg4,join(qnegra,bla2));
	char* reyna61 = join(reyna6,whiteSquare);

	char* reyna7 = join(qblanca,join(neg6,negro));
	char* reyna8 = join(neg2,join(negro,qblanca));	
	char* reyna81 = join(reyna8,neg4);

	char* tablero1 =up(reyna1,reyna21); 
	char* tablero2 =up(tablero1,reyna31);
	char* tablero3 = up(tablero2,reyna4);
	char* tablero4 = up(tablero3,reyna5);
	char* tablero5 = up(tablero4,reyna61);
	char* tablero6 = up(tablero5,reyna7);
	char* tablero7 = up(tablero6,reyna81);

 	interpreter(tablero7);


}

