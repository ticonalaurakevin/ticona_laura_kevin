#include "chess.h"
#include "figures.h"

void display(){
/*PIEZAS BLANCAS*/
	char* neg = repeatH(join(reverse(whiteSquare),whiteSquare),4);
	char* piezas1 = join(rook,join(knight,bishop));
	char* piezas0 = join(rook,join(flipV(knight),bishop));
	char* reyes = join(queen,king);
	char* piezas2 = join(piezas1,join(reyes,flipV(piezas0)));

	char* all = superImpose(piezas2,neg);
	
	char* bla = reverse(neg);
	char* peon = repeatH(pawn,8);
	char* peones = superImpose(peon,bla);

/* PIEZAS NEGRAS*/	
	char* piezas3 = join(piezas1,join(reyes,flipV(piezas1)));
	char* peonesn = flipH(reverse(peones)); 
	char* all2 = superImpose(piezas3,neg);
	char* piezasn = flipH(reverse(all2));

/*JUNTAMOS*/
	char* pblancos = up(all,peones);
	char* sinpiezas = repeatV(up(neg,bla),2);
	char* pnegras = up(peonesn,piezasn);

	char** tablero = up(sinpiezas,up(pblancos,pnegras));
 	interpreter(tablero);

}

